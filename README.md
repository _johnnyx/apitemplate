## RESTful API Template

Welcome to the RESTful API Template! This should be a good starting point for creating your first API and running it on any server, including your local machine.

Please follow these steps to get started

0. Make sure you have [NodeJS](https://nodejs.org/en/download/) and [GIT](https://git-scm.com/downloads) installed prior to continuing!
1. Cool, Now open your favorite code editor, mine is [this](https://www.jetbrains.com/webstorm/)
2. Clone this repository into your root projects folder, if this is your first project follow [this](https://git-scm.com/book/en/v1/Git-Basics-Getting-a-Git-Repository#Cloning-an-Existing-Repository) guide on cloning a repository or go to the end of this document.
3. Once cloned, run "npm install" to install the dependencies.
4. Open terminal and go to the root directory of this project, should looks something like this **/path/to/apitemplate**, and run "npm start"
5. The "server" should now be running and you can submit requests to http://localhost:3000/

---

## Submitting a request

Just like a good friend, Jake's the man

I highly recommend downloading a tool to help with testing API's. The tool that I use is called [Postman](https://www.getpostman.com/). This is a free tool that you can use even outside of this project including testing API's prior to implementing them into your project.

1. GET requests to "/" will respond with index route
2. GET requests to "/users" will respond with a list of users
2. GET requests to "/users/:id" where **id** is a unique identifier for a specific user, will respond with that user data
---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You'll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you'd like to and then click **Clone**.
4. Open the directory you just created to see your repository's files.

## Questions? Comments? Concerns?

Well i'm a therapist but if theres anything you want to get off your chest feel free to email me at johnny@johnnyx.com