let express = require('express'),
    router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

let object = {
  title: 'Your First RESTful API',
  image:
      { url: 'https://cdn.ebaumsworld.com/mediaFiles/picture/718392/85364631.jpeg',
        alt: 'Awkward'
      }
};

module.exports = router;
